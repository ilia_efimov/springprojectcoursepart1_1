package com.epam.springcource.dao;

import com.epam.springcource.dao.exceptions.DaoException;
import com.epam.springcource.dao.exceptions.DaoMemoryException;
import com.epam.springcource.dao.util.ReflectionTool;
import com.epam.springcource.transferobjects.BaseTransferObject;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public class MemoryDAO<T extends BaseTransferObject>
        extends BaseDAO<T> {

    private Map<String, T> memoryStorage = new HashMap<>();
    private long id = 0;

    public MemoryDAO() {

    }

    @Override
    protected String generateId(){
        id++;
        return getPrefix() == null ? String.format("%7d", id) : String.format("%s%7d", getPrefix(), id);
    }

    @Override
    public String addItem(T pEntity) throws DaoException {
        pEntity.setId(generateId());
        memoryStorage.put(pEntity.getId(), pEntity);
        return pEntity.getId();
    }

    @Override
    public void updateItem(T pEntity) throws DaoException {
        if (memoryStorage.containsKey(pEntity.getId())) {
            memoryStorage.replace(pEntity.getId(), pEntity);
        } else {
            throw new DaoMemoryException("Element has not found");
        }
    }

    @Override
    public void deleteItem(T pEntity) throws DaoException{
        if (memoryStorage.containsKey(pEntity.getId())) {
            memoryStorage.remove(pEntity.getId());
        } else {
            throw new DaoMemoryException("Element has not found");
        }
    }

    @Override
    public T getItem(String id) throws DaoException {
        if (memoryStorage.containsKey(id)) {
            return memoryStorage.get(id);
        } else {
            return null;
        }
    }

    @Override
    public List<T> getItemsByField(String pFieldName, Object pValue) throws DaoException {
        List<T> result = new ArrayList<>();
        try {
            for (T entry : memoryStorage.values()) {
                Object value = ReflectionTool.runGetter(pFieldName, entry);
                if (value != null && value.equals(pValue)) {
                    result.add(entry);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new DaoMemoryException(e.getMessage());
        }

        return result;
    }



    @Override
    public List<T> getItemsByExpression(String pExpression) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> returnAll() {
        return new ArrayList<>(memoryStorage.values());
    }
}
