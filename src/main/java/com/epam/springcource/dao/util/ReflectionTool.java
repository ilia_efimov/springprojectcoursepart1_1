package com.epam.springcource.dao.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Ilia_Efimov on 6/14/2017.
 */
public class ReflectionTool  {

    private static String getMethodName(String pFieldName) {
        if (pFieldName == null || pFieldName.length() < 1) {
            return null;
        }
        return "get" + pFieldName.substring(0,1).toUpperCase() + pFieldName.substring(1, pFieldName.length());
    }

    public static Method findGetterByFieldId(String pFieldname, Class pClass) {
        for (Class<?> c = pClass; c != null; c = c.getSuperclass()) {
            Method[] methods = c.getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().equals(getMethodName(pFieldname))) {
                    return method;
                }
            }
        }
        return null;
    }

    public static Object runGetter(Method pMethod, Object pObject) throws IllegalAccessException, InvocationTargetException {
        return pMethod.invoke(pObject);
    }

    public static Object runGetter(String pFieldName, Object pObject) throws IllegalAccessException, InvocationTargetException {
        Method method = findGetterByFieldId(pFieldName, pObject.getClass());
        return runGetter(method, pObject);
    }
}
