package com.epam.springcource.dao;

import com.epam.springcource.dao.exceptions.DaoException;
import com.epam.springcource.transferobjects.BaseTransferObject;

import java.util.List;

/**
 * Created by Ilia Efimov on 12.06.2017.
        */
public abstract class BaseDAO<E extends BaseTransferObject> {

    private String prefix = null;

    protected abstract String generateId();

    public abstract String addItem(E pEntity) throws DaoException;

    public abstract void updateItem(E pEntity) throws DaoException;

    public abstract void deleteItem(E pEntity) throws DaoException;

    public abstract E getItem(String id) throws DaoException;

    public abstract List<E> getItemsByField(String pFieldName, Object pValue) throws DaoException;

    public abstract List<E> getItemsByExpression(String pExpression);

    public abstract List<E> returnAll();

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
