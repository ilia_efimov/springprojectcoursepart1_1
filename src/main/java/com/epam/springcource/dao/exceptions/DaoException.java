package com.epam.springcource.dao.exceptions;

/**
 * Created by Ilia_Efimov on 6/14/2017.
 */
public class DaoException extends Exception {

    private String message;

    public DaoException(String pMessage) {
        this.message = pMessage;
    }

    @Override
    public String getMessage() {
        return String.format("DAO Exception with message: %s", message );
    }
}
