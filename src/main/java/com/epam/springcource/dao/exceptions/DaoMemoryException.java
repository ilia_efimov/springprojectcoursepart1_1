package com.epam.springcource.dao.exceptions;

/**
 * Created by Ilia_Efimov on 6/14/2017.
 */
public class DaoMemoryException extends DaoException {

    private String message;

    public DaoMemoryException(String pMessage) {
        super(pMessage);
    }

    @Override
    public String getMessage() {
        return  String.format("%s: DAO Memory exception [%s]", message);
    }
}
