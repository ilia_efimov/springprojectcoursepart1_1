package com.epam.springcource.services;

import com.epam.springcource.dao.BaseDAO;
import com.epam.springcource.dao.exceptions.DaoException;
import com.epam.springcource.transferobjects.User;

import java.util.List;

/**
 * Created by Ilia_Efimov on 6/14/2017.
 */
public class UserService {

    private BaseDAO<User> dao;

    public UserService(BaseDAO<User> pDao) {
        this.dao = pDao;
    }

    public String save(User pUser) {
        try {
            return dao.addItem(pUser);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void remove(User pUser) {
        try {
            dao.deleteItem(pUser);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    public User getById(String pId) {
        try {
            return dao.getItem(pId);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getByEmail(String pEmail) {
        try {
            List<User> users = dao.getItemsByField("email", pEmail);
            return users.get(0);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getAll() {
        return dao.returnAll();
    }
}
