package com.epam.springcource.services;

import com.epam.springcource.dao.BaseDAO;
import com.epam.springcource.dao.exceptions.DaoException;
import com.epam.springcource.transferobjects.Event;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ilia Efimov on 15.06.2017.
 */
public class EventService {
    private BaseDAO<Event> daoEvent;

    public EventService(BaseDAO<Event> pDao) {
        this.daoEvent = pDao;
    }

    public String save(Event pEvent) {
        try {
            return daoEvent.addItem(pEvent);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void remove(Event pEvent) {
        try {
            daoEvent.deleteItem(pEvent);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    public Event getById(String pId) {
        try {
            return daoEvent.getItem(pId);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Event getByEmail(String pName) {
        try {
            List<Event> users = daoEvent.getItemsByField("nameEvent", pName);
            return users.get(0);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Event> getAll() {
        return daoEvent.returnAll();
    }

    public List<Event> getForDateRange(Date pFrom, Date pTo) {
        List<Event> events = getAll();
        if (events.size() > 0) {
            return events.stream()
                    .filter(o -> (o.getEventStart().after(pFrom) && o.getEventStart().before(pTo)))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public List<Event> getNextEvents(Date pTo) {
        return getForDateRange(new Date(), pTo);
    }

}
