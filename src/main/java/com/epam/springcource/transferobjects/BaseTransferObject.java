package com.epam.springcource.transferobjects;

import java.io.Serializable;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public class BaseTransferObject implements Serializable {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
