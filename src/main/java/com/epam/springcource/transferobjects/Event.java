package com.epam.springcource.transferobjects;

import com.epam.springcource.generalclasses.RatingEnum;
import com.epam.springcource.generalclasses.beans.Place;

import java.util.Date;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public class Event extends BaseTransferObject {
    private String nameEvent;
    private long price;
    private RatingEnum rating;
    private Date eventStart;
    private Date eventEnd;
    private Place eventPlace;

    public String getNameEvent() {
        return nameEvent;
    }

    public void setNameEvent(String nameEvent) {
        this.nameEvent = nameEvent;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public RatingEnum getRating() {
        return rating;
    }

    public void setRating(RatingEnum rating) {
        this.rating = rating;
    }

    public Date getEventStart() {
        return eventStart;
    }

    public void setEventStart(Date eventStart) {
        this.eventStart = eventStart;
    }

    public Date getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(Date eventEnd) {
        this.eventEnd = eventEnd;
    }

    public Place getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(Place eventPlace) {
        this.eventPlace = eventPlace;
    }
}
