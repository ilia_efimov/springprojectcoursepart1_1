package com.epam.springcource.transferobjects;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public class User extends BaseTransferObject {
    private String userName;
    private String email;
    private String password;
    private String city;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
