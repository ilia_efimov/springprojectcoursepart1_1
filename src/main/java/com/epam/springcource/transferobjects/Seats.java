package com.epam.springcource.transferobjects;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public class Seats extends BaseTransferObject {
    private int rowNumber;
    private int columnNumber;
    private boolean vip;

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    public boolean getVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }
}
