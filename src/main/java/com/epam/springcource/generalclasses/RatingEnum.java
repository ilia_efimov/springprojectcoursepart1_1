package com.epam.springcource.generalclasses;

/**
 * Created by Ilia Efimov on 12.06.2017.
 */
public enum RatingEnum {
    HIGH,
    MID,
    LOW
}
