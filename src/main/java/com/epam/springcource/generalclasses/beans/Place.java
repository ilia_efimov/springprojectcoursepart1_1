package com.epam.springcource.generalclasses.beans;

import com.epam.springcource.transferobjects.Seats;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Ilia Efimov on 15.06.2017.
 */
public class Place {
    private String city;
    private String placeName;
    private int numberSeats;
    private List<Short> vipSeats;

    public Place(String pCity, String pPlaceName, String pVipSeats, int pNumberSeats) {
        this.city = pCity;
        this.placeName = pPlaceName;
        this.numberSeats = pNumberSeats;
        if (pVipSeats == null || pVipSeats.length() == 0) {
            return;
        }
        String[] stringSeats = pVipSeats.split(";");
        vipSeats = Stream.of(stringSeats)
                .map(o -> Short.parseShort(o))
                .collect(Collectors.toList());

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getNumberSeats() {
        return numberSeats;
    }

    public void setNumberSeats(int numberSeats) {
        this.numberSeats = numberSeats;
    }

    public List<Short> getVipSeats() {
        return vipSeats;
    }

    public void setVipSeats(List<Short> vipSeats) {
        this.vipSeats = vipSeats;
    }
}
