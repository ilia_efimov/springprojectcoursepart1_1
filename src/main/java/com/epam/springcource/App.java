package com.epam.springcource;

import com.epam.springcource.services.UserService;
import com.epam.springcource.transferobjects.User;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



/**
 * Hello world!
 *
 */
public class App 
{
    private static final ConfigurableApplicationContext ctx =
            new ClassPathXmlApplicationContext("spring-base.xml");

    public static void main( String[] args ) {
        UserService userService = (UserService) ctx.getBean("userService");


        User user = new User();
        user.setUserName("Jack Poll");
        user.setCity("Saratov");
        user.setEmail("test@test.com");
        user.setPassword("privet");

        userService.save(user);


        user = userService.getByEmail("test@test.com");
        System.out.println(user.getId());
        System.out.println(user.getUserName());
        System.out.println(user.getCity());
        System.out.println(user.getEmail());
        System.out.println(user.getPassword());

    }
}
